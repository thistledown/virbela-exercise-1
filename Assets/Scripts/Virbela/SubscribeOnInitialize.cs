using System.Collections;
using UnityEngine;

namespace Virbela
{
    /// <summary>
    /// Register as a CG object when spawned outside of the appropriate environment.
    /// </summary>
    public class SubscribeOnInitialize : MonoBehaviour
    {
        public ColorableManager Manager;
        void Start()
        {
            StartCoroutine(WaitForInit());
        }
        
        /// <summary>
        /// Wait a frame or so for the events to register.
        /// </summary>
        /// <returns>Nothing, 0.2 seconds later.</returns>
        private IEnumerator WaitForInit()
        {
            yield return new WaitForSeconds(0.2f);
            Colorable colorable = GetComponent<Colorable>();
            Manager.InvokeItemSpawn(colorable);
        }
    }
}
