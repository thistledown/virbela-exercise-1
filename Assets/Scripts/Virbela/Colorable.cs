using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virbela
{
    /// <summary>
    /// Generic colorable, which allows for easy coloring.
    /// </summary>
    public class Colorable : MonoBehaviour
    {
        /// <summary>
        /// The color when not highlighted.
        /// </summary>
        public Color RestColor = Color.black;
        
        /// <summary>
        /// The color when highlighted.
        /// </summary>
        public Color HighlightColor = Color.green;
        
        private Renderer _renderer;
        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
            Rest();
        }
        
        /// <summary>
        /// Change color to highlighted.
        /// </summary>
        public void Highlight()
        {
            _renderer.material.color = HighlightColor;
        }
        
        /// <summary>
        /// Change color to rest color.
        /// </summary>
        public void Rest()
        {
            _renderer.material.color = RestColor;
        }
    }
}