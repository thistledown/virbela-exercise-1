using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Virbela
{
    /// <summary>
    /// Manages Colorables and creates events for spawning them.
    /// </summary>
    public class ColorableManager : MonoBehaviour
    {
        /// <summary>
        /// An event to notify when items are spawned.
        /// </summary>
        public event EventHandler<Colorable> ItemSpawned;
        
        /// <summary>
        /// Item prefab to spawn.
        /// </summary>
        public GameObject itemPrefab;
        public GameObject botPrefab;

        private void Update()
        {
            // Doing input this way remains ridiculous, but Good Enough(tm) for this application.
            if (Input.GetKey(KeyCode.J))
            {
                SpawnItem();
            }
            if (Input.GetKey(KeyCode.K))
            {
                SpawnBot();
            }
        }

        /// <summary>
        /// Spawn the item prefab, raise the ItemSpawned event.
        /// </summary>
        void SpawnItem()
        {
            if (itemPrefab != null)
            {
                Vector2 position = Random.insideUnitCircle * 20f;
                GameObject go = Instantiate(itemPrefab, new Vector3(position.x,0f,position.y), Quaternion.identity);
                OnItemSpawned(go.GetComponent<Colorable>());
            }
        }
        /// <summary>
        /// Spawn the bot prefab, raise the ItemSpawned event.
        /// </summary>
        void SpawnBot()
        {
            if (itemPrefab != null)
            {
                Vector2 position = Random.insideUnitCircle * 20f;
                GameObject go = Instantiate(botPrefab, new Vector3(position.x,0f,position.y), Quaternion.identity);
                OnItemSpawned(go.GetComponent<Colorable>());
            }
        }

        public void InvokeItemSpawn(Colorable c)
        {
            OnItemSpawned(c);
        }
        
        /// <summary>
        /// Invoke the ItemSpawned event.
        /// </summary>
        /// <param name="c">An instance of Colorable.</param>
        protected virtual void OnItemSpawned(Colorable c)
        {
            ItemSpawned?.Invoke(this, c);
        }
    }
}