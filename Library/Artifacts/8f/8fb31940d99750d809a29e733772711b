                           2               2020.2.1f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                       \       ŕyŻ     `       ´!                                                                                                                                            ŕyŻ                                                                                    CullingEntity   @!  using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.Linq;

namespace Virbela 
{
    /// <summary>
    /// Culling Entity, aka: a player.
    /// </summary>
    public class CullingEntity : MonoBehaviour 
    {
        /*
        There are many different ways of going about this problem. This is not the best possible solution.
        Some other, better solutions involve KD trees or octrees, but I don't think that I could devise a from-scratch 
        implementation of either within a timespan of three days with any confidence in the sanity of either data 
        structure. Ideally in this case, I would use KD trees specifically, as there is a very, very good implementation
        of it in the public domain -- far better than I could do myself, unless given a much larger time constraint. Or
        a DOTS-based octree, which would be even faster.

        However, this solution scales reasonably well. By using culling groups, unless our entities are packed extremely 
        densely, we can use an extremely simple LINQ query on our inner band to find the closest object with a much more
        narrow list than we would if we just used it on the entire collection. In addition, culling groups are event 
        driven, which is lovely.
        
        There are other pitfalls with this approach, though -- namely, it only works with static objects for "bots" and
        "items" in this case. It's not DRY. It probably could be, if totally refactored.
        It does not work correctly in the editor itself -- only when the game window is open, too. For both,
        split-screen is required. This is because culling groups work using the camera. It will fail when there is
        nothing in the inner inclusion radius. This could be mitigated by expanding it when there is nothing. Because
        it works via arrays, and default(int) is 0, it will not correctly get the 0th element, and default to it.
        This could be mitigated by having a blank, offscreen 0th element, or another large refactor.
        
        On the upside, it can handle over a thousand objects while still maintaining >200FPS on my low-end machine. I
        also think it's a rather clever solution myself, being that the overhead is very small indeed.
        
        */

        /// <summary>
        /// Distance for the outer edge of the inner band of the culling group.
        /// </summary>
        public float includeDistance;
        
        /// <summary>
        /// Frequency to update distance at, in seconds.
        /// </summary>
        public float updateFrequency = 0.25f;
        
        /// <summary>
        /// The colorable manager to listen for.
        /// </summary>
        public ColorableManager colorableManager;
        
        
        private float maxDistance = 1000f;
        
        private List<Colorable> _colorables;
        private CullingGroup _cullingGroup;
        private List<BoundingSphere> _bounds;
        private CharacterController _controller;
        private int _lastClosestItem = -1;
        private int _lastClosestBot = -1;
        int[] _closest = new int[1000];

        private void Start()
        {
            _colorables = new List<Colorable>();
            _bounds = new List<BoundingSphere>();
            _controller = GetComponent<CharacterController>();
            // Subscribe to ItemSpawned events.
            if(colorableManager != null) colorableManager.ItemSpawned += OnItemSpawned;
            // Create a culling group, make sure our camera is its reference, and use this object as its center point.
            _cullingGroup = new CullingGroup();
            _cullingGroup.targetCamera = Camera.main;
            _cullingGroup.SetDistanceReferencePoint(transform);
            // We end our culling distance at ~1km because it's a reasonable distance.
            _cullingGroup.SetBoundingDistances(new float[] { includeDistance, maxDistance });
            StartCoroutine(QueryDistances());
        }

        private void Update()
        {
            _controller.SimpleMove(new Vector3(Input.GetAxis("Horizontal")*10f,0f, Input.GetAxis("Vertical")*10f));
        }

        IEnumerator QueryDistances()
        {
            while (true)
            {
                
                // This could probably be moved to the CG event.
                int queryLength = _cullingGroup.QueryIndices(0, _closest, 0);
                if (queryLength == 0) yield return new WaitForSeconds(updateFrequency);
                
                
                // O(n log n)
                var bandZero = _closest.ToList()
                    .OrderBy(i => (_colorables[i].transform.position - transform.position).sqrMagnitude);

                // Try-catch because FirstOrDefault is rude and returns default(int), which is 0 and not helpful.
                try
                {
                    // the most awful hack but seriously stop it
                    _colorables[0].transform.position = Vector3.negativeInfinity; 
                    // Grab the first Item named as a clone, and highlight it. O(n), but small size.
                    int? closestItem = bandZero.First(item => _colorables[item].CompareTag("Item"));
                    _colorables[closestItem.GetValueOrDefault()].Highlight();
                    // And de-highlight it when it's no longer the focus.
                    if (closestItem != _lastClosestItem && _lastClosestItem != -1) _colorables[_lastClosestItem].Rest();
                    _lastClosestItem = closestItem.GetValueOrDefault();
                    
                }
                catch (Exception e)
                {
                    // Rest when we're something not an invalid value.
                    if (_lastClosestItem != -1) _colorables[_lastClosestItem].Rest();
                    _lastClosestItem = -1;
                }

                try
                {
                    // Grab the first cloned bot, highlight it.
                    int? closestBot = bandZero.First(item => _colorables[item].CompareTag("Bot"));
                    _colorables[closestBot.GetValueOrDefault()].Highlight();
                    // and de-highlight it when it's no longer the focus.
                    if (closestBot != _lastClosestBot && _lastClosestBot != -1) _colorables[_lastClosestBot].Rest();
                    _lastClosestBot = closestBot.GetValueOrDefault();
                }
                catch (Exception e)
                {
                    if (_lastClosestBot != -1) _colorables[_lastClosestBot].Rest();
                    _lastClosestBot = -1;
                }
                
                yield return new WaitForSeconds(updateFrequency);
            }
        }
        
        /// <summary>
        /// Process the ItemSpawned event by adding the Colorable to _colorables and adding the correct bounds to _bounds.
        /// </summary>
        /// <param name="sender">The sender's generic object.</param>
        /// <param name="c">A Colorable.</param>
        public void OnItemSpawned(object sender, Colorable c)
        {
            if (c == null) return;
            _colorables.Add(c);
            // Create a bounding sphere that encapsulates the general size of the mesh for our CullingGroup.
            BoundingSphere bounds = new BoundingSphere();
            bounds.position = c.transform.position;
            bounds.radius = c.transform.GetComponent<MeshFilter>().mesh.bounds.extents.x;
            _bounds.Add(bounds);
            /* Probably a necessary evil by this implementation. If our objects could not change, we could just
             use an array. collection.ToArray is O(n), so if we had a truly huge amount of objects, it would be 
             very bad in this case, but I'll take allocation inefficiency over per-frame inefficiency. */
            _cullingGroup.SetBoundingSpheres(_bounds.ToArray());
            _cullingGroup.SetBoundingSphereCount(_bounds.Count);
        }
        /// <summary>
        /// Get rid of the culling group manually because it does not hit the GC normally.
        /// </summary>
        private void OnDestroy()
        {
            _cullingGroup.Dispose();
            _cullingGroup = null;
        }

    }
}
                       CullingEntity      Virbela 