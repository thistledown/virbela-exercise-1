                           "               2020.2.1f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                       \       ŕyŻ     `       0                                                                                                                                            ŕyŻ                                                                                    CullingEntity   ť  using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Virbela 
{
    public class CullingEntity : MonoBehaviour 
    {
        /*
        There are many different ways of going about this problem. This is not the best possible solution.
        Some other, better solutions involve KD trees or octrees, but I don't think that I could devise a from-scratch 
        implementation of either within a timespan of three days with any confidence in the sanity of either data 
        structure. Ideally in this case, I would use KD trees specifically, as there is a very, very good implementation
        of it in the public domain -- far better than I could do myself, unless given a much larger time constraint.

        However, this solution scales reasonably well. By using culling groups, unless our entities are packed extremely 
        densely, we can use an extremely simple LINQ query on our inner band to find the closest object with a much more
        narrow list than we would if we just used it on the entire collection. In addition, culling groups are event 
        driven, which is lovely.
        */

        /// <summary>
        /// Distance for the outer edge of the inner band of the culling group.
        /// </summary>
        public float includeDistance;

        /// <summary>
        /// The colorable manager to listen for.
        /// </summary>
        public ColorableManager colorableManager;
        
        
        private float maxDistance = 5f;
        
        private List<Colorable> _colorables;
        private CullingGroup _cullingGroup;
        private List<BoundingSphere> _bounds;
        

        private void Start()
        {
            _colorables = new List<Colorable>();
            _bounds = new List<BoundingSphere>();
            // Subscribe to ItemSpawned events.
            if(colorableManager != null) colorableManager.ItemSpawned += OnItemSpawned;
            // Create a culling group, make sure our camera is its reference, and use this object as its center point.
            _cullingGroup = new CullingGroup();
            _cullingGroup.targetCamera = Camera.main;
            _cullingGroup.SetDistanceReferencePoint(transform);
            // We end our culling distance at ~1km because it's a reasonable distance.
            _cullingGroup.SetBoundingDistances(new float[] { includeDistance, maxDistance });
            
        }

        /// <summary>
        /// When a culling group event is processed, highlight or un-highlight.
        /// </summary>
        /// <param name="e">A CullingGroupEvent.</param>
        public void OnStateChanged(CullingGroupEvent e)
        {
            Debug.Log(e.currentDistance);
            if(e.currentDistance == 0)
                _colorables[e.index].Highlight();
            else
                _colorables[e.index].Rest();
        }
        /// <summary>
        /// Process the ItemSpawned event by adding the Colorable to _colorables and adding the correct bounds to _bounds.
        /// </summary>
        /// <param name="sender">The sender's generic object.</param>
        /// <param name="c">A Colorable.</param>
        public void OnItemSpawned(object sender, Colorable c)
        {
            if (c == null) return;
            _colorables.Add(c);
            // Create a bounding sphere that encapsulates the general size of the mesh for our CullingGroup.
            BoundingSphere bounds = new BoundingSphere();
            bounds.position = c.transform.position;
            bounds.radius = c.transform.GetComponent<MeshFilter>().mesh.bounds.extents.x;
            _bounds.Add(bounds);
            /* Probably a necessary evil by this implementation. If our objects could not change, we could just
             use an array. collection.ToArray is O(n), so if we had a truly huge amount of objects, it would be 
             very bad in this case, but I'll take allocation inefficiency over per-frame inefficiency. */
            _cullingGroup.SetBoundingSpheres(_bounds.ToArray());
            _cullingGroup.SetBoundingSphereCount(_bounds.Count);
            _cullingGroup.onStateChanged -= OnStateChanged;
            _cullingGroup.onStateChanged += OnStateChanged;
        }
        private void OnDestroy()
        {
            
            _cullingGroup.Dispose();
            _cullingGroup = null;
        }

    }
}
                        CullingEntity      Virbela 